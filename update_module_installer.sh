#!/bin/bash

#
# ipdev @ xda-developers


# Set main variables
TDIR=$(pwd)
INSTALL=$TDIR/META-INF/com/google/android


[[ ! -d $INSTALL ]] && mkdir -p $INSTALL

curl -o $INSTALL/update-binary https://raw.githubusercontent.com/topjohnwu/Magisk/cf47214ee4912ed1538fbea3d09ba9dd9b5746b0/scripts/module_installer.sh

[[ ! -f $INSTALL/updater-script ]] && echo "#MAGISK" > $INSTALL/updater-script

exit 0;
